import 'dart:io';
import 'Bos.dart';
import 'Exit.dart';
import 'Food.dart';
import 'Player.dart';
import 'TableMap.dart';
import 'Tree.dart';
import 'Zombie.dart';

void main() {
  TableMap map = new TableMap(21, 21);
  Player player = new Player(2, 2, '\u{1F9CD}', map, 30, 5);
  Bos bos = new Bos(5, 5);
  Exit exit = new Exit(19, 19);

  //zombie
  map.addObj(new Zombie(1, 4));
  map.addObj(new Zombie(2, 6));
  map.addObj(new Zombie(3, 1));
  map.addObj(new Zombie(4, 5));
  map.addObj(new Zombie(5, 9));
  map.addObj(new Zombie(6, 10));
  map.addObj(new Zombie(7, 8));
  map.addObj(new Zombie(8, 15));
  map.addObj(new Zombie(9, 14));
  map.addObj(new Zombie(10, 13));
  map.addObj(new Zombie(11, 5));
  map.addObj(new Zombie(12, 2));
  map.addObj(new Zombie(13, 10));
  map.addObj(new Zombie(14, 8));
  map.addObj(new Zombie(15, 15));
  map.addObj(new Zombie(16, 1));
  map.addObj(new Zombie(17, 9));
  // map.addObj(new Zombie(18, 20));
  map.addObj(new Zombie(19, 16));
  // map.addObj(new Zombie(14, 20));
  map.addObj(new Zombie(19, 18));
  map.addObj(new Zombie(13, 18));
  map.addObj(new Zombie(14, 19));
  // map.addObj(new Zombie(16, 20));

  // //tree
  map.addObj(new Tree(10, 7));
  map.addObj(new Tree(10, 6));
  map.addObj(new Tree(10, 8));
  map.addObj(new Tree(10, 9));
  map.addObj(new Tree(10, 10));
  map.addObj(new Tree(11, 8));
  map.addObj(new Tree(9, 8));
  map.addObj(new Tree(8, 8));
  map.addObj(new Tree(1, 15));
  map.addObj(new Tree(2, 15));
  map.addObj(new Tree(2, 16));
  map.addObj(new Tree(2, 17));
  map.addObj(new Tree(2, 18));
  map.addObj(new Tree(2, 14));

  // food
  map.addObj(new Food(6, 3, 5));
  map.addObj(new Food(6, 15, 5));
  map.addObj(new Food(12, 12, 5));
  map.addObj(new Food(4, 9, 5));

  //set
  map.setBos(bos);
  map.setPlayer(player);
  map.setExit(exit);

  stdout.writeln('Welcome to zombie city escape game.');
  stdout.writeln('>> The game is loading. Wait a moment!! <<');
  try {
    while (true) {
      stdout.writeln('---------------------------------------');
      map.showMap();
      // a, w, s, d ; q quit
      stdout.writeln('Choose direction(a,w,s,d,q=exit): ');
      String str = stdin.readLineSync();
      var direction = str[0];
      if (direction == 'q') {
        stdout.write('Bye bye...');
        break;
      } else {
        player.walk(direction);
      }
    }
  } catch (e) {
    stdout.write('Game error!!!');
  }
}
