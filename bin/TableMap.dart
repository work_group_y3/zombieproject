import 'dart:io';

import 'Bos.dart';
import 'Exit.dart';
import 'Food.dart';
import 'Obj.dart';
import 'Player.dart';
import 'Tree.dart';
import 'Zombie.dart';

class TableMap {
  int width;
  int height;
  int objCount = 0;
  Player player;
  Bos bos;
  Exit exit;
  var objects = <Obj>[];

  TableMap(int width, int height) {
    this.width = width;
    this.height = height;
  }

  void addObj(Obj obj) {
    objects.add(obj);
    objCount++;
  }

  void setPlayer(Player player) {
    this.player = player;
    addObj(player);
  }

  void setBos(Bos bos) {
    this.bos = bos;
    addObj(bos);
  }

  void setExit(Exit exit) {
    this.exit = exit;
    addObj(exit);
  }

  void showMap() {
    stdout.writeln("City");
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        var symbol = '- ';
        if (y == 0 && x < width) {
          symbol = '# ';
        } else if (y < height && x == 0) {
          symbol = '# ';
        } else if (y < height && x == width - 1) {
          symbol = '#';
        } else if (y + 1 == height && x + 1 < width) {
          symbol = '# ';
        }
        for (int o = 0; o < objCount; o++) {
          if (objects[o].isOn(x, y)) {
            symbol = objects[o].getSymbol();
          }
        }
        stdout.write(symbol);
      }
      stdout.writeln("");
    }
    stdout.writeln(player);
    stdout.writeln('---------------------------------------');
  }

  inMap(int x, int y) {
    if ((x > 0 && x < width) && (y > 0 && y < height)) {
      return true;
    } else {
      return false;
    }
  }

  isBos(int x, int y) {
    if (bos.isOn(x, y)) {
      return true;
    } else {
      return false;
    }
  }

  isExit(int x, int y) {
    if (exit.isOn(x, y)) {
      return true;
    } else {
      return false;
    }
  }

  bool isTree(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Tree && objects[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  isZombie(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Zombie && objects[o].isOn(x, y)) {
        return player.reduceHp();
      }
    }
    return false;
  }

  int fillFood(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Food && objects[o].isOn(x, y)) {
        Food food = objects[o];
        return food.fillFood();
      }
    }
    return 0;
  }
}
