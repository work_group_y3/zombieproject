import 'Obj.dart';

class Exit extends Obj {
  Exit(int x, int y) : super('\u{1F306}', x, y);
}
