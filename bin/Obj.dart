import 'dart:io';

class Obj {
  int x;
  int y;
  var symbol;

  Obj(var symbol, int x, int y) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
  }

  getX() {
    return x;
  }

  getY() {
    return y;
  }

  getSymbol() {
    return symbol;
  }

  isOn(int x, int y) {
    return this.x == x && this.y == y;
  }
}
