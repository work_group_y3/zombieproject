import 'dart:io';

import 'Obj.dart';
import 'TableMap.dart';

class Player extends Obj {
  int food;
  int hp;
  TableMap map;

  Player(int x, int y, var symbol, TableMap map, int food, int hp)
      : super(symbol, x, y) {
    this.symbol = '\u{1F9CD}';
    this.map = map;
    this.food = food;
    this.hp = hp;
  }

  bool canWalk(int x, int y) {
    return food > 0 && map.inMap(x, y) && !map.isTree(x, y) && hp > 0;
  }

  void reduceHunger() {
    food--;
  }

  void reduceHp() {
    hp--;
  }

  void chcekFood() {
    int food = map.fillFood(x, y);
    if (food > 0) {
      this.food += food;
    } else if (this.food == 0) {
      stdout.writeln('You dead from malnutrition.');
      return exit(0);
    }
  }

  void chcekHp() {
    if (this.hp == 0) {
      stdout.writeln('You lost too much blood to death.');
      return exit(0);
    }
  }

  walk(var direction) {
    switch (direction) {
      case 'w':
        chcekFood();
        chcekHp();

        if (canWalk(x, y - 1)) {
          y = y - 1;
          reduceHunger();
        } else {
          return false;
        }
        // y = y - 1;
        break;
      case 's':
        chcekFood();
        chcekHp();

        if (canWalk(x, y + 1)) {
          y = y + 1;
          reduceHunger();
          map.isZombie(x, y);
        } else {
          return false;
        }
        // y = y + 1;
        break;
      case 'd':
        chcekFood();
        chcekHp();

        if (canWalk(x + 1, y)) {
          x = x + 1;
          reduceHunger();
          map.isZombie(x, y);
        } else {
          return false;
        }
        // x = x + 1;
        break;
      case 'a':
        chcekFood();
        chcekHp();

        if (canWalk(x - 1, y)) {
          x = x - 1;
          reduceHunger();
          map.isZombie(x, y);
        } else {
          return false;
        }
        // x = x - 1;
        break;
      default:
        return false;
    }
    if (map.isBos(x, y)) {
      stdout.writeln('---------------------------------------');
      map.showMap();
      stdout.writeln(">>> Get killed by the (boss), end the game.");
      stdout.write('Bye bye...');
      exit(0);
    }
    if (map.isExit(x, y)) {
      stdout.writeln('---------------------------------------');
      stdout.writeln('^_^ You have left this city.');
      stdout.write('<< Zombie Project >>');
      exit(0);
    }
    return true;
  }

  @override
  String toString() {
    return "Player: " + "p(X=$x, Y=$y ; HP=$hp ; Hunger=$food)";
  }
}
